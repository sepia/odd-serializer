# ODD-serializer

## About

This module creates HTML pages in a [SADE](http://sade.textgrid.de) compliant design for a digital edition's ODD, thus enabling projects to comfortably display their metadata schema on their website.

It has originally been developed for the DFG-funded project Bibliothek der Neologie ([website](http://bdn-edition.de) | [repositories](https://gitlab.gwdg.de/bibliothek-der-neologie)) and spun off as a generic app for [SADE](http://sade.textgrid.de).

## Requirements

The ODD-Serializer needs the [SADE Code-Viewer](https://gitlab.gwdg.de/SADE/code-viewer) to be installed in order to work properly.


## Installation

The ODD-Serializer is **not** shipped with SADE by default.
To install this app, please make sure you set the parameter `odd.installation` in your SADE build's `$project.build.properties` to `true`.
Confer the ["Bibliothek der Neologie" build config](https://gitlab.gwdg.de/bibliothek-der-neologie/bdn-build/blob/develop/neologie-dev.build.properties) for usage.


## Usage

To use the package simply include it in your XQuery via import with

```
import module namespace odd-serializer="http://bdn-edition.de/ns/odd-serializer";
```

To generate a HTML page, call

```
odd-serializer:main($path-to-file, $coll-to-store-html)
```

with

* `$path-to-file` being the **absolute** path to your schema documentation
* `$coll-to-store-html` being the **absolute** collection path where you want to store your HTML page


## Contributing

Feel free to contribute to this repo by sending us a pull request! Please make sure you stick to the [RDD style guides](https://github.com/subugoe/rdd-technical-reference/blob/master/rdd-technical-reference.md) for XQuery.

## License

See  the [LICENSE](https://gitlab.gwdg.de/SADE/odd-serializer/blob/develop/LICENSE) file.
