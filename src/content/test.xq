xquery version "3.1";

(: prefix is unused but must be bound for xqsuite :)
import module namespace odd-test="http://sepia.io/odd-serializer/test";
import module namespace test="http://exist-db.org/xquery/xqsuite"
at "resource:org/exist/xquery/lib/xqsuite/xqsuite.xql";


test:suite(util:list-functions("http://sepia.io/odd-serializer/test"))
