## [2.2.2](https://gitlab.gwdg.de/sepia/odd-serializer/compare/v2.2.1...v2.2.2) (2025-03-05)


### Bug Fixes

* set correct app strings ([a5ad399](https://gitlab.gwdg.de/sepia/odd-serializer/commit/a5ad399089d4b6ed1bcefa35df5dc432f29b9fb1))

## [2.2.1](https://gitlab.gwdg.de/sepia/odd-serializer/compare/v2.2.0...v2.2.1) (2025-03-05)


### Bug Fixes

* path to dependencies file ([24b7ba6](https://gitlab.gwdg.de/sepia/odd-serializer/commit/24b7ba6df1ab8696889954cbf383bf4a0a3cff67))

# [2.2.0](https://gitlab.gwdg.de/sepia/odd-serializer/compare/v2.1.0...v2.2.0) (2025-03-05)


### Features

* upload artifact to xar repo ([1494706](https://gitlab.gwdg.de/sepia/odd-serializer/commit/14947067ce66198bafb763fa819d6e7e09abadb5))

# [2.1.0](https://gitlab.gwdg.de/sepia/odd-serializer/compare/v2.0.0...v2.1.0) (2025-03-05)


### Bug Fixes

*  err:XQST0034 in test.xqm ([75e43b2](https://gitlab.gwdg.de/sepia/odd-serializer/commit/75e43b2e7e8eaeb2728041973f21ee06a5273a3c))
* deactivate husky ([c52fa81](https://gitlab.gwdg.de/sepia/odd-serializer/commit/c52fa81eef2b719b7b5ca55798ea695dd4b67561))


### Features

* towards ci build w/o docker ([66473f5](https://gitlab.gwdg.de/sepia/odd-serializer/commit/66473f5ad3284d9794c09789d952c196f1bbddea))

## [2.0.1](https://gitlab.gwdg.de/sepia/odd-serializer/compare/v2.0.0...v2.0.1) (2025-03-05)


### Bug Fixes

*  err:XQST0034 in test.xqm ([75e43b2](https://gitlab.gwdg.de/sepia/odd-serializer/commit/75e43b2e7e8eaeb2728041973f21ee06a5273a3c))

# [2.0.0](https://gitlab.gwdg.de/sepia/odd-serializer/compare/v1.0.0...v2.0.0) (2021-07-05)


### Features

* **library:** transform package into a library ([713a0d6](https://gitlab.gwdg.de/sepia/odd-serializer/commit/713a0d6bd6c1fb18000f0579750cdaca5ff90b25)), closes [#1](https://gitlab.gwdg.de/sepia/odd-serializer/issues/1)


### BREAKING CHANGES

* **library:** namespace changed to `http://sepia.io/odd-serializer/main`
